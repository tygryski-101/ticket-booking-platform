@extends('layouts.app')



@section('main-content')
    @include('layouts.partials.header')
<section id="contact_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title_section">Contact</h2>
                <div class="contact_content_page">
                    <div class="screen-reader-response"></div>
                    <div class="contact_content_block_page">
                        <h3>Airlines.com</h3>
                        <p><a href="mailto:airlines@gmail.com">airlines@gmail.com</a></p>
                        <p><a href="tel:+48111222333">+48 111 222 333</a></p>
                    </div>
                    <div class="contact_content_block_page ">
                        <form action="{{url('send-mail')}}" method="post" name="contact-form" id="contact-form">
                            {{csrf_field()}}
                            <div class="input_group smart">
                                <span class="inputName">
                                    <input type="text" name="inputName" value="" size="40" id="inputName" required placeholder="Name">
                                </span>
                            </div>
                            <div class="input_group smart">
                                <span class="inputEmail">
                                    <input type="email" name="inputEmail" value="" size="40" id="inputEmail" required placeholder="Email">
                                </span>
                            </div>
                            <div class="input_group">
                                <span class="inputPhone">
                                    <input type="tel" name="inputPhone" value="" size="40" id="inputPhone" aria-invalid="false" placeholder="Phone">
                                </span>
                            </div>
                            <div class="input_group smart">
                                <span class="inputmessage">
                                    <textarea name="inputmessage" cols="40" rows="4" id="inputmessage" required placeholder="Message"></textarea>
                                </span>
                            </div>

                            <p>
                                <input type="submit" value="SEND">
                                <span class="ajax-loader"><img src="{{asset('/img/ajax-loader.gif')}}" alt="loader"></span>
                            </p>
                        </form>
                        <div class="sent_msg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="find_me">
    <div class="container">
        <div class="col-md-12">
            <h2 class="title_section_find_me">Find Us</h2>
        </div>
    </div><!-- End  container -->
    <div class="bg_transparent">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2562.5688646641447!2d22.024086851510695!3d50.03817397931962!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473cfaefdc15c509%3A0xac40657a51285281!2sLwowska+15A%2C+Rzesz%C3%B3w!5e0!3m2!1suk!2spl!4v1495836323525" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>
    @include('layouts.partials.footer')
@endsection