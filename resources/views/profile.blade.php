@extends('layouts.app')

@include('layouts.partials.css_auth')

@section('main-content')
    @include('layouts.partials.header')


    <div class="success_delete modal fade" style="display: none;">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ticket_title_delete">Success</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center">The reservation was cancelled successfully!</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade ticket_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ticket_title_delete">cancel the ticket</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to cancel the ticket?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success ok">Accept</button>
                    <button type="button" class="btn btn-danger cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="alert" role="alert" id="result"></div>
    <section id="profile_user">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="ticket_title">Profile</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{--<div class="panel-heading">Profile</div>--}}
                        <div class="row">
                            <div class="col-sm-3 col-md-3">
                                @if($user->sex == 'male')
                                    <img class="photo_user" src="{{asset('img/male.png')}}" alt="male">
                                @else
                                    <img class="photo_user" src="{{asset('img/female.png')}}" alt="male">
                                @endif
                            </div>
                            <div class="col-sm-9 col-md-9">
                                <div class="profile_content">
                                    <blockquote>
                                        <p>{{$user->name}} {{$user->surname}}</p> <small><cite title="Source Title">Rzeszów, Poland</cite></small>
                                    </blockquote>
                                    <p>{{$user->email}}]
                                        <br />January 30, 1974</p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        @if(isset($tickets_airlines))
            <div class="row">
                <div class="col-md-12">
                    @if($tickets_airlines != [])
                        <h3 class="ticket_title your_tickets">Your Tickets</h3>
                    @else
                        <h3 class="ticket_title your_tickets">You have no tickets</h3>
                    @endif
                </div>
            </div>
            <div class="row">
            @foreach($tickets_airlines as $item)
                    <div class="col-md-12 col-sm-6">
                        <div class="tickets">
                    <div class="row no-pad">
                        <div class="col-md-offset-2 col-md-1">
                            <div class="ticket_left">
                                <div class="vertical">
                                    <p>This is a dummy boarding pass</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="ticket_center">
                                <h4>Air Ticket</h4>
                                <table>
                                    <tr>
                                        <td><span>name</span><p>{{$item['ticket']->name}} {{$item['ticket']->surname}}</p></td>
                                        {{--<td colspan="4" class="seat">seat</td>--}}
                                    </tr>
                                    <tr>
                                        <td><span>flight</span><p>{{$item['airline_instance']->plane->name}}</p></td>
                                        <td><span>date</span><p>{{date("Y-m-d", strtotime($item['airline_instance']->departure_time))}}</p></td>
                                        <td><span>time</span><p>{{date("H:i", strtotime($item['airline_instance']->departure_time))}}</p></td>
                                        <td><span>class</span><p>
                                                @if($item['airline_instance']->plane->seats_business_count >= $item['ticket']->seat_number)
                                                    2
                                                    @else
                                                    1
                                                @endif
                                            </p></td>
                                        <td>
                                            <span>origin</span>
                                            <p>{{$item['country_from']->name}} / {{$item['city_from']->name}}</p>
                                            <span>destination</span>
                                            <p>{{$item['country_to']->name}} / {{$item['city_to']->name}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" ><img src="{{asset('img/example_QR.jpg')}}" alt=""></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="ticket_right">
                                <h4>Boarding pass</h4>
                                <div class="ticket_right_border">
                                    <table>
                                        <tr>
                                            <td><span>name </span><strong>{{$item['ticket']->name}} {{$item['ticket']->surname}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td><span>date </span><strong>{{date("H:i", strtotime($item['airline_instance']->departure_time))}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>flight </span><strong>{{$item['airline_instance']->plane->name}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>origin </span><strong>{{$item['city_from']->name}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>destination </span><strong>{{$item['city_to']->name}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><img style="text-align: right" src="{{asset('img/example_QR.jpg')}}" alt=""></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <button class="btn btn-danger btn-lg pull-right cancel_ticket"
                                    type="button"
                                    data-method="delete"
                                    data-action="delete"
                                    data-id={{$item['ticket']->id}}
                                    data-token="{{csrf_token()}}"
                            >Сancel a ticket</button>
                        </div>
                    </div>

                </div>
                    </div>
            @endforeach
            </div>
        @endif
    </div>
    </section>
@include('layouts.partials.footer')
@endsection