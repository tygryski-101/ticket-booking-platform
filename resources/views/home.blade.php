@extends('layouts.app')



@section('main-content')
@include('layouts.partials.header')

<section id="content_search">
    <div class="container">
        <div class="row">
            <div class="booking-form">
                {{ Form::open(['url' => 'preloader', 'class' => 'search_form', 'id' => 'check_validation', "data-location"=>"preloder", 'method' => 'POST']) }}

                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-checkbox">
                        <label for="one-way">
                            <input type="radio" class="destination" name="one_two_airline" id="one-way" value="1">
                            <span></span>One way
                        </label>
                        <label for="roundtrip">
                            <input type="radio" class="destination" name="one_two_airline" id="roundtrip" value="2" checked>
                            <span></span>Two way
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flying from</span>
                                <input type="text" class="form-control {{ $errors->has('from_city') ? 'has-error' :'' }}" autocomplete="off" name="from_city" id="from_city" placeholder="City or airport" required>

                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flyning to</span>
                                <input type="text" class="form-control {{ $errors->has('to_city') ? 'has-error' :'' }}" autocomplete="off" name="to_city" id="to_city" placeholder="City or airport" required>
                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Departing</span>
                            <input type="text" class="form-control {{ $errors->has('data_from') ? 'has-error' :'' }}" name="data_from" id="data_from" placeholder="Date From" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="data_to">
                            <div class="form-group">
                                <span class="form-label">Returning</span>
                                <input type="text" class="form-control {{ $errors->has('data_to') ? 'has-error' :'' }}" name="data_to" id="data_to" placeholder="Date To">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Adults (18+)</span>
                            <select class="form-control" name="class_client">
                                <option value="1" selected>Any class</option>
                                <option value="2">Business class</option>
                                <option value="2">Economy class</option>
                            </select>
                            <span class="select-arrow"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-btn">
                        <button type="submit" class="submit-btn">Show flights</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

</section>

<section id="news_main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news_slider">
                    <div class="slide">
                        <img src="{{ asset('img/slider5.jpg')}}" alt="slider1">
                        <div class="desc_slide">
                            <h3>promo</h3>
                            <p>Promo description</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider2.jpg')}}" alt="slider2">
                        <div class="desc_slide">
                            <h3>promo 2</h3>
                            <p>Promo description 2</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider3.jpg')}}" alt="slider3">
                        <div class="desc_slide">
                            <h3>promo 3</h3>
                            <p>Promo description 3</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider4.jpg')}}" alt="slider4">
                        <div class="desc_slide">
                            <h3>promo 4</h3>
                            <p>Promo description 4</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider5.jpg')}}" alt="slider5">
                        <div class="desc_slide">
                            <h3>promo 5</h3>
                            <p>Promo description 5</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider6.jpg')}}" alt="slider6">
                        <div class="desc_slide">
                            <h3>promo 6</h3>
                            <p>Promo description 6</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="popular">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="popular_airline">
                    <h3>TOP 10 AIRLINES</h3>
                    <ul>
                        <li>
                            <a href=""><span>1. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>2. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>3. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>4. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>5. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>6. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>7. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div><!-- popular_airline -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="popular_direction">
                    <h3>POPULAR DESTINATION</h3>
                    <ul>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>

                    </ul>
                </div><!-- popular_direction -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="container_popular_city">
                    <h3 class="title_popular_city">POPULAR CITY</h3>
                    <div class="popular_city">
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/warsaw.jpg')}}" alt="warsaw">
                            <div class="desc_slide_city">
                                <h3>Warsawa</h3>
                                <p>Poland</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                    </div>
                </div><!-- container_popular_city -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="advantage">
                    <h3 class="title_advantage">The ADVANTAGES of the airline</h3>
                    <div class="advantage_container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <h3>The largest portfolio of travel services</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </div>
                                        <h3>Customer support at all stages of purchase</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </div>
                                        <h3>Bonus program for regular customers</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <h3>Cheap flights</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <h3>The first independent rating of airlines</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                        </div>
                                        <h3>Dynamics of prices for air travel</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section><!-- section popular -->@extends('layouts.app')



@section('main-content')
@include('layouts.partials.header')



<section id="content_search">
    <div class="container">
        <div class="row">
            <div class="booking-form">
                {{ Form::open(['url' => 'preloader', 'class' => 'search_form', 'id' => 'check_validation', "data-location"=>"preloder", 'method' => 'POST']) }}

                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-checkbox">
                        <label for="one-way">
                            <input type="radio" class="destination" name="one_two_airline" id="one-way" value="1">
                            <span></span>One way
                        </label>
                        <label for="roundtrip">
                            <input type="radio" class="destination" name="one_two_airline" id="roundtrip" value="2" checked>
                            <span></span>Two way
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flying from</span>
                                <input type="text" class="form-control {{ $errors->has('from_city') ? 'has-error' :'' }}" autocomplete="off" name="from_city" id="from_city" placeholder="City or airport" required>

                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flyning to</span>
                                <input type="text" class="form-control {{ $errors->has('to_city') ? 'has-error' :'' }}" autocomplete="off" name="to_city" id="to_city" placeholder="City or airport" required>
                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Departing</span>
                            <input type="text" class="form-control {{ $errors->has('data_from') ? 'has-error' :'' }}" name="data_from" id="data_from" placeholder="Date From" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="data_to">
                            <div class="form-group">
                                <span class="form-label">Returning</span>
                                <input type="text" class="form-control {{ $errors->has('data_to') ? 'has-error' :'' }}" name="data_to" id="data_to" placeholder="Date To">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Adults (18+)</span>
                            <select class="form-control" name="class_client">
                                <option value="1" selected>Any class</option>
                                <option value="2">Business class</option>
                                <option value="2">Economy class</option>
                            </select>
                            <span class="select-arrow"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-btn">
                        <button type="submit" class="submit-btn">Show flights</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

</section>

<section id="news_main">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="news_slider">
                    <div class="slide">
                        <img src="{{ asset('img/slider1.jpg')}}" alt="slider1">
                        <div class="desc_slide">
                            <h3>promo</h3>
                            <p>Promo description</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider2.jpg')}}" alt="slider2">
                        <div class="desc_slide">
                            <h3>promo 2</h3>
                            <p>Promo description 2</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider3.jpg')}}" alt="slider3">
                        <div class="desc_slide">
                            <h3>promo 3</h3>
                            <p>Promo description 3</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider4.jpg')}}" alt="slider4">
                        <div class="desc_slide">
                            <h3>promo 4</h3>
                            <p>Promo description 4</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider5.jpg')}}" alt="slider5">
                        <div class="desc_slide">
                            <h3>promo 5</h3>
                            <p>Promo description 5</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider6.jpg')}}" alt="slider6">
                        <div class="desc_slide">
                            <h3>promo 6</h3>
                            <p>Promo description 6</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="application_mobile">
                    <img src="{{ asset('img/mobile_banner.jpg')}}" alt="mobile_banner">
                    <h3>Flights to your smartphone</h3>
                    <div class="mobile_button">
                        <a href="#"><img src="{{ asset('img/app_store_btn.png')}}" alt="apple_store"></a>
                        <a href="#"><img src="{{ asset('img/google_play_btn.png')}}" alt="google_play"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="popular">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="popular_direction">
                    <h3>POPULAR DESTINATION</h3>
                    <ul>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>

                    </ul>
                </div><!-- popular_direction -->
                <div class="container_popular_city">
                    <h3 class="title_popular_city">POPULAR CITY</h3>
                    <div class="popular_city">
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/warsaw.jpg')}}" alt="warsaw">
                            <div class="desc_slide_city">
                                <h3>Warsawa</h3>
                                <p>Poland</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                    </div>
                </div><!-- container_popular_city -->
                <div class="advantage">
                    <h3 class="title_advantage">The ADVANTAGES of the airline</h3>
                    <div class="advantage_container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <h3>The largest portfolio of travel services</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </div>
                                        <h3>Customer support at all stages of purchase</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </div>
                                        <h3>Bonus program for regular customers</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <h3>Cheap flights</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <h3>The first independent rating of airlines</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                        </div>
                                        <h3>Dynamics of prices for air travel</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="popular_airline">
                    <h3>TOP 10 AIRLINES</h3>
                    <ul>
                        <li>
                            <a href=""><span>1. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>2. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>3. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>4. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>5. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>6. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>7. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>8. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>9. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>10. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div><!-- popular_airline -->
                <div class="subscribe">
                    <img src="{{ asset('img/subscribe_bg.png')}}" alt="subscribe">
                    <h3>Subscribe and be always up to date!</h3>
                    <ul>
                        <li>Airline discounts</li>
                        <li>New direction</li>
                        <li>The best price</li>
                    </ul>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" id="subscribe_email" placeholder="Enter email">
                        </div>
                        <button type="submit"><i class="fa fa-envelope-o" aria-hidden="true"></i>Subscribe</button>
                    </form>
                </div><!-- subscribe -->
            </div>
        </div>
    </div>
</section><!-- section popular -->@extends('layouts.app')



@section('main-content')
@include('layouts.partials.header')



<section id="content_search">
    <div class="container">
        <div class="row">
            <div class="booking-form">
                {{ Form::open(['url' => 'preloader', 'class' => 'search_form', 'id' => 'check_validation', "data-location"=>"preloder", 'method' => 'POST']) }}

                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-checkbox">
                        <label for="one-way">
                            <input type="radio" class="destination" name="one_two_airline" id="one-way" value="1">
                            <span></span>One way
                        </label>
                        <label for="roundtrip">
                            <input type="radio" class="destination" name="one_two_airline" id="roundtrip" value="2" checked>
                            <span></span>Two way
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flying from</span>
                                <input type="text" class="form-control {{ $errors->has('from_city') ? 'has-error' :'' }}" autocomplete="off" name="from_city" id="from_city" placeholder="City or airport" required>

                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flyning to</span>
                                <input type="text" class="form-control {{ $errors->has('to_city') ? 'has-error' :'' }}" autocomplete="off" name="to_city" id="to_city" placeholder="City or airport" required>
                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Departing</span>
                            <input type="text" class="form-control {{ $errors->has('data_from') ? 'has-error' :'' }}" name="data_from" id="data_from" placeholder="Date From" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="data_to">
                            <div class="form-group">
                                <span class="form-label">Returning</span>
                                <input type="text" class="form-control {{ $errors->has('data_to') ? 'has-error' :'' }}" name="data_to" id="data_to" placeholder="Date To">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Adults (18+)</span>
                            <select class="form-control" name="class_client">
                                <option value="1" selected>Any class</option>
                                <option value="2">Business class</option>
                                <option value="2">Economy class</option>
                            </select>
                            <span class="select-arrow"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-btn">
                        <button type="submit" class="submit-btn">Show flights</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

</section>

<section id="news_main">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="news_slider">
                    <div class="slide">
                        <img src="{{ asset('img/slider1.jpg')}}" alt="slider1">
                        <div class="desc_slide">
                            <h3>promo</h3>
                            <p>Promo description</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider2.jpg')}}" alt="slider2">
                        <div class="desc_slide">
                            <h3>promo 2</h3>
                            <p>Promo description 2</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider3.jpg')}}" alt="slider3">
                        <div class="desc_slide">
                            <h3>promo 3</h3>
                            <p>Promo description 3</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider4.jpg')}}" alt="slider4">
                        <div class="desc_slide">
                            <h3>promo 4</h3>
                            <p>Promo description 4</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider5.jpg')}}" alt="slider5">
                        <div class="desc_slide">
                            <h3>promo 5</h3>
                            <p>Promo description 5</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider6.jpg')}}" alt="slider6">
                        <div class="desc_slide">
                            <h3>promo 6</h3>
                            <p>Promo description 6</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="application_mobile">
                    <img src="{{ asset('img/mobile_banner.jpg')}}" alt="mobile_banner">
                    <h3>Flights to your smartphone</h3>
                    <div class="mobile_button">
                        <a href="#"><img src="{{ asset('img/app_store_btn.png')}}" alt="apple_store"></a>
                        <a href="#"><img src="{{ asset('img/google_play_btn.png')}}" alt="google_play"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="popular">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="popular_direction">
                    <h3>POPULAR DESTINATION</h3>
                    <ul>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>

                    </ul>
                </div><!-- popular_direction -->
                <div class="container_popular_city">
                    <h3 class="title_popular_city">POPULAR CITY</h3>
                    <div class="popular_city">
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/warsaw.jpg')}}" alt="warsaw">
                            <div class="desc_slide_city">
                                <h3>Warsawa</h3>
                                <p>Poland</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                    </div>
                </div><!-- container_popular_city -->
                <div class="advantage">
                    <h3 class="title_advantage">The ADVANTAGES of the airline</h3>
                    <div class="advantage_container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <h3>The largest portfolio of travel services</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </div>
                                        <h3>Customer support at all stages of purchase</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </div>
                                        <h3>Bonus program for regular customers</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <h3>Cheap flights</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <h3>The first independent rating of airlines</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                        </div>
                                        <h3>Dynamics of prices for air travel</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="popular_airline">
                    <h3>TOP 10 AIRLINES</h3>
                    <ul>
                        <li>
                            <a href=""><span>1. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>2. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>3. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>4. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>5. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>6. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>7. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>8. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>9. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>10. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div><!-- popular_airline -->
                <div class="subscribe">
                    <img src="{{ asset('img/subscribe_bg.png')}}" alt="subscribe">
                    <h3>Subscribe and be always up to date!</h3>
                    <ul>
                        <li>Airline discounts</li>
                        <li>New direction</li>
                        <li>The best price</li>
                    </ul>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" id="subscribe_email" placeholder="Enter email">
                        </div>
                        <button type="submit"><i class="fa fa-envelope-o" aria-hidden="true"></i>Subscribe</button>
                    </form>
                </div><!-- subscribe -->
            </div>
        </div>
    </div>
</section><!-- section popular -->@extends('layouts.app')



@section('main-content')
@include('layouts.partials.header')



<section id="content_search">
    <div class="container">
        <div class="row">
            <div class="booking-form">
                {{ Form::open(['url' => 'preloader', 'class' => 'search_form', 'id' => 'check_validation', "data-location"=>"preloder", 'method' => 'POST']) }}

                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-checkbox">
                        <label for="one-way">
                            <input type="radio" class="destination" name="one_two_airline" id="one-way" value="1">
                            <span></span>One way
                        </label>
                        <label for="roundtrip">
                            <input type="radio" class="destination" name="one_two_airline" id="roundtrip" value="2" checked>
                            <span></span>Two way
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flying from</span>
                                <input type="text" class="form-control {{ $errors->has('from_city') ? 'has-error' :'' }}" autocomplete="off" name="from_city" id="from_city" placeholder="City or airport" required>

                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="from_to">
                            <div class="marker form-group">
                                <span class="form-label">Flyning to</span>
                                <input type="text" class="form-control {{ $errors->has('to_city') ? 'has-error' :'' }}" autocomplete="off" name="to_city" id="to_city" placeholder="City or airport" required>
                                <div id='loadingmessage'>
                                    <img src='{{asset('img/giphy.gif')}}' />
                                </div>
                                <div class="result">
                                    <select class="select_from_to" multiple>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Departing</span>
                            <input type="text" class="form-control {{ $errors->has('data_from') ? 'has-error' :'' }}" name="data_from" id="data_from" placeholder="Date From" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="data_to">
                            <div class="form-group">
                                <span class="form-label">Returning</span>
                                <input type="text" class="form-control {{ $errors->has('data_to') ? 'has-error' :'' }}" name="data_to" id="data_to" placeholder="Date To">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span class="form-label">Adults (18+)</span>
                            <select class="form-control" name="class_client">
                                <option value="1" selected>Any class</option>
                                <option value="2">Business class</option>
                                <option value="2">Economy class</option>
                            </select>
                            <span class="select-arrow"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-btn">
                        <button type="submit" class="submit-btn">Show flights</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

</section>

<section id="news_main">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="news_slider">
                    <div class="slide">
                        <img src="{{ asset('img/slider1.jpg')}}" alt="slider1">
                        <div class="desc_slide">
                            <h3>promo</h3>
                            <p>Promo description</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider2.jpg')}}" alt="slider2">
                        <div class="desc_slide">
                            <h3>promo 2</h3>
                            <p>Promo description 2</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider3.jpg')}}" alt="slider3">
                        <div class="desc_slide">
                            <h3>promo 3</h3>
                            <p>Promo description 3</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider4.jpg')}}" alt="slider4">
                        <div class="desc_slide">
                            <h3>promo 4</h3>
                            <p>Promo description 4</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider5.jpg')}}" alt="slider5">
                        <div class="desc_slide">
                            <h3>promo 5</h3>
                            <p>Promo description 5</p>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('img/slider6.jpg')}}" alt="slider6">
                        <div class="desc_slide">
                            <h3>promo 6</h3>
                            <p>Promo description 6</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="application_mobile">
                    <img src="{{ asset('img/mobile_banner.jpg')}}" alt="mobile_banner">
                    <h3>Flights to your smartphone</h3>
                    <div class="mobile_button">
                        <a href="#"><img src="{{ asset('img/app_store_btn.png')}}" alt="apple_store"></a>
                        <a href="#"><img src="{{ asset('img/google_play_btn.png')}}" alt="google_play"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="popular">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="popular_direction">
                    <h3>POPULAR DESTINATION</h3>
                    <ul>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Warsaw<span class="direction-arrow">→</span>Toronto
                                <span class="button_buy_ticets">
                                    <span>
                                        <i>from 145</i>
                                        <i>USD</i>
                                    </span>
                                    <span>Buy a ticket</span>
                                </span>
                            </a>
                        </li>

                    </ul>
                </div><!-- popular_direction -->
                <div class="container_popular_city">
                    <h3 class="title_popular_city">POPULAR CITY</h3>
                    <div class="popular_city">
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/warsaw.jpg')}}" alt="warsaw">
                            <div class="desc_slide_city">
                                <h3>Warsawa</h3>
                                <p>Poland</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/paris.jpg')}}" alt="paris">
                            <div class="desc_slide_city">
                                <h3>Paris</h3>
                                <p>France</p>
                            </div>
                        </div>
                        <div class="slide_city">
                            <img src="{{ asset('img/moscow.jpg')}}" alt="moscow">
                            <div class="desc_slide_city">
                                <h3>Moscow</h3>
                                <p>Russia</p>
                            </div>
                        </div>
                    </div>
                </div><!-- container_popular_city -->
                <div class="advantage">
                    <h3 class="title_advantage">The ADVANTAGES of the airline</h3>
                    <div class="advantage_container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <h3>The largest portfolio of travel services</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </div>
                                        <h3>Customer support at all stages of purchase</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </div>
                                        <h3>Bonus program for regular customers</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <h3>Cheap flights</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <h3>The first independent rating of airlines</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="advantage_block">
                                    <div class="title_icon">
                                        <div class="advantage_block_icon">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                        </div>
                                        <h3>Dynamics of prices for air travel</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="popular_airline">
                    <h3>TOP 10 AIRLINES</h3>
                    <ul>
                        <li>
                            <a href=""><span>1. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>2. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>3. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>4. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>5. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>6. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>7. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>8. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>9. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>10. </span>Airlines Soulplane
                                <span class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div><!-- popular_airline -->
                <div class="subscribe">
                    <img src="{{ asset('img/subscribe_bg.png')}}" alt="subscribe">
                    <h3>Subscribe and be always up to date!</h3>
                    <ul>
                        <li>Airline discounts</li>
                        <li>New direction</li>
                        <li>The best price</li>
                    </ul>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" id="subscribe_email" placeholder="Enter email">
                        </div>
                        <button type="submit"><i class="fa fa-envelope-o" aria-hidden="true"></i>Subscribe</button>
                    </form>
                </div><!-- subscribe -->
            </div>
        </div>
    </div>
</section><!-- section popular -->



@include('layouts.partials.footer')
@endsection



@include('layouts.partials.footer')
@endsection



@include('layouts.partials.footer')
@endsection



@include('layouts.partials.footer')
@endsection