@extends('layouts.app')



@section('main-content')
    @include('layouts.partials.header_preloder')



    <div class="cssload-loader"></div>
    <section class="preloder_container">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="icon">
                        <img src="img/icon_airplane.png" alt="icon_airplane">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="content_preloader">
                        @if($airlines['one_two_airline'] == 2)
                                <ul class="to">
                                    <li><strong>{{$airlines['from_city']}},</strong> {{$airlines['from_country']}}</li>
                                    <li><i class="fa fa-plane" aria-hidden="true"></i></li>
                                    <li><strong>{{$airlines['to_city']}},</strong> {{$airlines['to_country']}}</li>
                                    <li><strong>Date:</strong> {{$airlines['data_from']}}</li>
                                </ul>
                                <ul class="back">
                                    <li><strong>{{$airlines['to_city']}},</strong> {{$airlines['to_country']}}</li>
                                    <li><i class="fa fa-plane" aria-hidden="true"></i></li>
                                    <li><strong>{{$airlines['from_city']}},</strong> {{$airlines['from_country']}}</li>
                                    <li><strong>Date:</strong> {{$airlines['data_to']}}</li>
                                </ul>
                        @else
                                <ul class="to">
                                    <li><strong>{{$airlines['from_city']}},</strong> {{$airlines['from_country']}}</li>
                                    <li><i class="fa fa-plane" aria-hidden="true"></i></li>
                                    <li><strong>{{$airlines['to_city']}},</strong> {{$airlines['to_country']}}</li>
                                    <li><strong>Date:</strong> {{$airlines['data_from']}}</li>
                                </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('layouts.partials.footer_preloder')
@endsection