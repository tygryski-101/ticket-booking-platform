@extends('layouts.app')



@section('main-content')
    @include('layouts.partials.header')

    <section class="cart">
        <div class="container">
            <div class="row">
                <div class="col cart_container">
                    @if(isset($airlines) && empty(!$airlines))
                        <form action="{{route('checkout')}}" method="post" id="checkout-form">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Customize your way</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Enter passenger data</a>
                                </li>
                                {{--<li class="nav-item">--}}
                                    {{--<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Wybierz metodę płatności</a>--}}
                                {{--</li>--}}
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <h3><i class="fa fa-plane" aria-hidden="true"></i>Travel plan</h3>
                                    @foreach($airlines as $airline)

                                        <table class="table table_economy">
                                            <tbody>
                                            <tr>
                                                <td colspan="4">
                                                    <strong>Flight of the day {{date("F j, Y", strtotime($airline['item']['departure_time']))}} from {{$airline['item']['from_city']}} to {{$airline['item']['to_city']}}</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <ul>
                                                        <li>
                                                            <input type="hidden" name="class_[{{$airline['item']['id']}}]" value="">
                                                            <input type="radio" data-class="class_[{{$airline['item']['id']}}]" name="airline_[{{$airline['item']['id']}}]" id="check_airline" value="{{$airline['item']['price_economy']}}" checked>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>departure: {{date("H:i", strtotime($airline['item']['departure_time']))}}</li>
                                                        <li>arrival: {{date("H:i", strtotime($airline['item']['arrival_time']))}}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>{{$airline['item']['from_city']}}</li>
                                                        <li>{{$airline['item']['to_city']}}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>Economy class</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>{{$airline['item']['plane_name']}}</li>
                                                        <li>{{$airline['item']['plane_number']}}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>Price: {{$airline['item']['price_economy']}}</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table_bisiness">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <ul>
                                                        <li>
                                                            <input type="hidden" name="class_[{{$airline['item']['id']}}]" value="">
                                                            <input type="radio" data-class="class_[{{$airline['item']['id']}}]" name="airline_[{{$airline['item']['id']}}]" id="check_airline" value="{{$airline['item']['price_business']}}">
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>departure: {{date("H:i", strtotime($airline['item']['departure_time']))}}</li>
                                                        <li>arrival: {{date("H:i", strtotime($airline['item']['arrival_time']))}}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>{{$airline['item']['from_city']}}</li>
                                                        <li>{{$airline['item']['to_city']}}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>Business class</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>{{$airline['item']['plane_name']}}</li>
                                                        <li>{{$airline['item']['plane_number']}}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        <li>Price: {{$airline['item']['price_business']}}</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endforeach

                                    <h3><i class="fa fa-suitcase" aria-hidden="true"></i>To add luggage for travel</h3>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td colspan="4">
                                                <strong>You can buy additional baggage at the airport</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="bag" id="bag_free" value="0" checked>
                                                            <label class="form-check-label" for="exampleRadios1">
                                                                Free
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="bag" id="bag_100" value="100">
                                                            <label class="form-check-label" for="exampleRadios2">
                                                                30 USD
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>The size of luggage: 42x32x25</li>
                                                    <li>The size of luggage: 56x45x25</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>The weight of the luggage: to 8kg</li>
                                                    <li>The weight of the luggage: from 8kg</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3><i class="fa fa-money" aria-hidden="true"></i>The cost of the trip</h3>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td colspan="4">
                                                <strong>The total cost of the trip (tickets, baggage)</strong>
                                            </td>
                                        </tr>
                                        <tr class="last_price">
                                            <td>
                                                <ul>
                                                    <li>A plane ticket</li>
                                                    <li>Luggage for travel</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>Price: <strong class="all_price">145</strong></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row justify-content-center">
                                        <div class="continue_profile">
                                            <div class="form-group">
                                                <label for="first_name">First Name</label>
                                                <input type="text" name="first_name" class="form-control" id="first_name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" name="last_name" class="form-control" id="last_name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email address:</label>
                                                <input type="email" name="email" class="form-control" id="email" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="tel">Phone:</label>
                                                <input type="tel" name="tel" class="form-control"  id="tel" required>
                                            </div>
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a href="{{route('check.profile')}}" class="btn btn-primary my_tab" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Continue</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="row">
                                        <div class="col-md-6 offset-md-3">
                                            <h1>Checkout</h1>
                                            <h4>Your Total: <strong class="all_price"></strong>$</h4>
                                                    <div class="form-group">
                                                        <label for="name">Name</label>
                                                        <input type="text" name="name" id="name" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <input type="text" name="address" id="address" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="card-name">Card Holder Name</label>
                                                        <input type="text" name="card-name" id="card-name" class="form-control" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="card-number">Credit Card Number</label>
                                                        <input type="text" id="card-number" class="form-control" required>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="card-expiry-month">Expiration Month</label>
                                                                <input type="text" name="card-expiry-month" id="card-expiry-month" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="card-expiry-year">Expiration Year</label>
                                                                <input type="text" name="card-expiry-year" id="card-expiry-year" class="form-control" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="card-cvc">CVC</label>
                                                        <input type="text" name="card-cvc" id="card-cvc" class="form-control" required>
                                                    </div>
                                            {{csrf_field()}}
                                            <button type="submit" class="btn btn-success">Buy now</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @else
                        @if(isset($bought))
                            <h2>{{$message}}</h2>
                        @else
                            <h2>No direction selected</h2>
                        @endif

                    @endif

                    <script>
                        function checkRadioInput(){
                            var sum = 0;
                            $('input:checked').each(function(i,elem) {
                                sum += +$(this).val();
                            });
                            $('.all_price').html(sum);
                        }
                        (function () {
                            checkRadioInput();
                        })();
                        $('input[type=radio]').change(function() {
                            checkRadioInput();
                        });
                    </script>
                </div>
                <div class="col-4 popular_airline">
                    <h3>TOP 10 AIRLINES</h3>
                    <ul>
                        <li>
                            <a href=""><span>1. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>2. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>3. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>4. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>5. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>6. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>7. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>8. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>9. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                        <li>
                            <a href=""><span>10. </span>Airlines Soulplane
                                <span class="rating">
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star" aria-hidden="true"></i>
										<i class="fa fa-star-half-o" aria-hidden="true"></i>
										<i class="fa fa-star-o" aria-hidden="true"></i>
									</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- popular_airline -->

            </div>
        </div>
    </section>

    @include('layouts.partials.footer')
@endsection