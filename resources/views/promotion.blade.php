@extends('layouts.app')



@section('main-content')
@include('layouts.partials.header')

<div id="promotion">
    <div id="topContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 offset-md-1 text-center">
                    <h1>Services</h1>
                    <h2>Text for promotion</h2>
                </div>
            </div>
        </div>
    </div>

    <div id="centerContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <div class="oneContent">
                        <div class="image">
                            <img class="d-none d-sm-block" src="{{asset('img/cirkle.png')}}">
                            <p class="d-none d-sm-block sale">25%</p>
                            <div>
                                <h3>Discount for Pensioners</h3>
                                <p> Are you over the age of 60?
                                    <br>Just show your ID card with a photo (e.g. Czech national ID card) and a discount of 30% is yours.
                                    <br>Only applicable to Economy class.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- TODO add more  -->
    </div>

    @include('layouts.partials.footer')
    @endsection