@extends('layouts.app')



@section('main-content')
    @include('layouts.partials.header')


    <section id="result_top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="result_top_content">
                        <a href="">To change the search condition</a>
                        <ul>
                            @if($old_airlines['one_two_airline'] == 2)
                                <li class="to">
                                    <span>{{$old_airlines['from_city']}}</span>
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    <span>{{$old_airlines['to_city']}}, </span>
                                    <span>Date: {{$old_airlines['data_from']}}</span>
                                </li>
                                <li>
                                    <span>{{$old_airlines['to_city']}}</span>
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    <span>{{$old_airlines['from_city']}}, </span>
                                    <span>Date: {{$old_airlines['data_to']}}</span>
                                </li>
                            @else
                                <li class="to">
                                    <span>{{$old_airlines['from_city']}}</span>
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    <span>{{$old_airlines['to_city']}}, </span>
                                    <span>Date: {{$old_airlines['data_from']}}</span>
                                </li>
                            @endif
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="result">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if($old_airlines['one_two_airline'] == 2)
                        <form action="" class="result_one">
                            <table class="table">
                                <thead>
                                <tr>
                                    <h4>
                                        <span class="from">{{$old_airlines['from_city']}}</span>
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        <span class="to">{{$old_airlines['to_city']}}</span>
                                    </h4>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airlines_from as $item )
                                    <tr>
                                        <td>
                                            <p><i class="fa fa-plane" aria-hidden="true"></i></p>
                                            <span class="rating">
                                                        <?php $random = rand(1, 5); ?>
                                                @for ($i=0; $i<5; $i++)
                                                    @if($i<$random)
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    @else
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    @endif
                                                @endfor
                                                    </span>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Flight <strong>{{$item['plane_number']}}</strong></li>
                                                <li>{{$item['plane_name']}}</li>
                                                <li>Economy/Business class</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>departure <strong>{{date("H:i", strtotime($item['departure_time']))}}</strong></li>
                                                <li>{{date("m.d.y", strtotime($item['departure_time']))}}</li>
                                                <li>{{$item['from_city']}}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>In the way <strong>{{date("H:i", strtotime($item['arrival_time']) - strtotime($item['departure_time']))}}</strong></li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>arrival <strong>{{date("H:i", strtotime($item['arrival_time']))}}</strong></li>
                                                <li>{{date("m.d.y", strtotime($item['arrival_time']))}}</li>
                                                <li>{{$item['to_city']}}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Average delay: 1 min</li>
                                                <li><i class="fa fa-briefcase" aria-hidden="true"></i>Hand luggage only</li>
                                                <li><i class="fa fa-suitcase" aria-hidden="true"></i>Luggage: <span>not included</span></li>
                                            </ul>
                                        </td>
                                        <?php
                                        $i = 0;
                                        $count = isset($checked) ? count($checked) : 0;
                                        $sum = 0;
                                        ?>
                                        @if($count > 0)
                                            @foreach($checked as $check_id)
                                                @if($check_id == $item['id'])
                                                    <?php $sum++; ?>
                                                    <td>
                                                        <a class="add_to_cart add_complite" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                    <span class="button_buy_ticets">
                                                                        <span>
                                                                            <i>from {{$item['price_economy']}}</i>
                                                                            <i>USD</i>
                                                                        </span>
                                                                        <span>Choose</span>
                                                                    </span>
                                                        </a>
                                                        <a class="delete_to_cart delete_complite" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                            <span>Cancel</span>
                                                        </a>
                                                    </td>


                                                @elseif($i == $count - 1 && $check_id != $item['id'] && $sum < 1 )
                                                    {{--{{dd(1)}}--}}
                                                    <td>
                                                        <a class="add_to_cart" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                        </a>
                                                        <a class="delete_to_cart" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                            <span>Cancel</span>
                                                        </a>
                                                    </td>
                                                @endif
                                                <?php $i++; ?>
                                            @endforeach
                                        @else
                                            <td>
                                                <a class="add_to_cart" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                </a>
                                                <a class="delete_to_cart" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                    <span>Cancel</span>
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </form>

                        <form action="" class="result_two">
                            <table class="table">
                                <thead>
                                <tr>
                                    <h4>
                                        <span class="from">{{$old_airlines['to_city']}}</span>
                                        <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                        <span class="to">{{$old_airlines['from_city']}}</span>
                                    </h4>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airlines_to as $item )
                                    <tr>
                                        <td>
                                            <p><i class="fa fa-plane" aria-hidden="true"></i></p>
                                            <span class="rating">
												<?php $random = rand(1, 5); ?>
                                                @for ($i=0; $i<5; $i++)
                                                    @if($i<$random)
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    @else
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    @endif
                                                @endfor
                                        </span>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Flight <strong>{{$item['plane_number']}}</strong></li>
                                                <li>{{$item['plane_name']}}</li>
                                                <li>Economy/Business class</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>departure <strong>{{date("H:i", strtotime($item['departure_time']))}}</strong></li>
                                                <li>{{date("m.d.y", strtotime($item['departure_time']))}}</li>
                                                <li>{{$item['from_city']}}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>In the way <strong>{{date("H:i", strtotime($item['arrival_time']) - strtotime($item['departure_time']))}}</strong></li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>arrival <strong>{{date("H:i", strtotime($item['arrival_time']))}}</strong></li>
                                                <li>{{date("m.d.y", strtotime($item['arrival_time']))}}</li>
                                                <li>{{$item['to_city']}}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Average delay: 1 min</li>
                                                <li><i class="fa fa-briefcase" aria-hidden="true"></i>Hand luggage only</li>
                                                <li><i class="fa fa-suitcase" aria-hidden="true"></i>Luggage: <span>not included</span></li>
                                            </ul>
                                        </td>
                                        <?php
                                        $i = 0;
                                        $count = isset($checked) ? count($checked) : 0;
                                        $sum = 0;
                                        ?>
                                        @if($count > 0)
                                            @foreach($checked as $check_id)
                                                @if($check_id == $item['id'])
                                                    <?php $sum++; ?>

                                                    <td>
                                                        <a class="add_to_cart add_complite" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                        </a>
                                                        <a class="delete_to_cart delete_complite" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                            <span>Cancel</span>
                                                        </a>
                                                    </td>


                                                @elseif($i == $count - 1 && $check_id != $item['id'] && $sum < 1 )
                                                    <td>
                                                        <a class="add_to_cart" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                            <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                        </a>
                                                        <a class="delete_to_cart" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                            <span>Cancel</span>
                                                        </a>
                                                    </td>
                                                @endif
                                                <?php $i++; ?>
                                            @endforeach
                                        @else
                                            <td>
                                                <a class="add_to_cart" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                            <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                </a>
                                                <a class="delete_to_cart" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                    <span>Cancel</span>
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </form>
                    @else
                        <form action="" class="result_one">
                            <table class="table">
                                <thead>
                                <tr>
                                    <h4>
                                        <span class="from">{{$old_airlines['from_city']}}</span>
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        <span class="to">{{$old_airlines['to_city']}}</span>
                                    </h4>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airlines_from as $item )
                                    <tr>
                                        <td>
                                            <p><i class="fa fa-plane" aria-hidden="true"></i></p>
                                            <span class="rating">
                                                        <?php $random = rand(1, 5); ?>
                                                @for ($i=0; $i<5; $i++)
                                                    @if($i<$random)
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    @else
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    @endif
                                                @endfor
                                                    </span>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Flight <strong>{{$item['plane_number']}}</strong></li>
                                                <li>{{$item['plane_name']}}</li>
                                                <li>Economy/Business class</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>departure <strong>{{date("H:i", strtotime($item['departure_time']))}}</strong></li>
                                                <li>{{date("m.d.y", strtotime($item['departure_time']))}}</li>
                                                <li>{{$item['from_city']}}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>In the way <strong>{{date("H:i", strtotime($item['arrival_time']) - strtotime($item['departure_time']))}}</strong></li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>arrival <strong>{{date("H:i", strtotime($item['arrival_time']))}}</strong></li>
                                                <li>{{date("m.d.y", strtotime($item['arrival_time']))}}</li>
                                                <li>{{$item['to_city']}}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Average delay: 1 min</li>
                                                <li><i class="fa fa-briefcase" aria-hidden="true"></i>Hand luggage only</li>
                                                <li><i class="fa fa-suitcase" aria-hidden="true"></i>Luggage: <span>not included</span></li>
                                            </ul>
                                        </td>
                                        <?php
                                        $i = 0;
                                        $count = isset($checked) ? count($checked) : 0;
                                        $sum = 0;
                                        ?>
                                        @if($count > 0)
                                            @foreach($checked as $check_id)
                                                @if($check_id == $item['id'])
                                                    <?php $sum++; ?>

                                                    <td>
                                                        <a class="add_to_cart add_complite" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                    <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                        </a>
                                                        <a class="delete_to_cart delete_complite" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                            <span>Cancel</span>
                                                        </a>
                                                    </td>


                                                @elseif($i == $count - 1 && $check_id != $item['id'] && $sum < 1 )
                                                    <td>
                                                        <a class="add_to_cart" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                        </a>
                                                        <a class="delete_to_cart" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                            <span>Cancel</span>
                                                        </a>
                                                    </td>
                                                @endif
                                                <?php $i++; ?>
                                            @endforeach
                                        @else
                                            <td>
                                                <a class="add_to_cart" href="{{URL::to('/add-to-cart/' . $item['id'])}}">
                                                                <span class="button_buy_ticets">
                                                                    <span>
                                                                        <i>from {{$item['price_economy']}}</i>
                                                                        <i>USD</i>
                                                                    </span>
                                                                    <span>Choose</span>
                                                                </span>
                                                </a>
                                                <a class="delete_to_cart" href="{{URL::to('/delete-to-cart/' . $item['id'])}}">
                                                    <span>Cancel</span>
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </form>
                    @endif
                        <a href="{{url('/cart')}}" class="btn btn-continue pull-right">Check Cart</a>
                </div>
            </div>
        </div>
    </section>


    @include('layouts.partials.footer')
@endsection