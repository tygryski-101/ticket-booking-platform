<div class="wrapper">
    <div class="content">
        <header class="main-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="logo">
                            <a href="{{url('/')}}">
                                <img src="{{asset('img/logo.png')}}" alt="logo">
                                <!-- <span>soul plane</span> -->
                            </a>
                            <h3>Airlines</h3>
                        </div>
                    </div>
                </div>
            </div>
        </header>