<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="navigation">
                    <nav>
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="{{url('promotion')}}">Promotions</a></li>
                            <li><a href="{{url('insurance')}}">Insurance</a></li>
                            <li><a href="{{url('contact')}}">Contact</a></li>
                            <li>
                                <a href="{{route('airlineCart')}}">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart
                                    @if(Session::has('cart'))
                                        <span class="badge">{{ (Session::get('cart')->totalQty != 0) ? Session::get('cart')->totalQty : '' }}</span>
                                    @else
                                        <span class="badge"></span>
                                    @endif

                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-12">
                <div class="payment">
                    <div class="icon">
                        <img src="{{asset('img/payment_png/1.png')}}" alt="">
                        <img src="{{asset('img/payment_png/2.png')}}" alt="">
                        <img src="{{asset('img/payment_png/3.png')}}" alt="">
                        <img src="{{asset('img/payment_png/4.png')}}" alt="">
                        <img src="{{asset('img/payment_png/5.png')}}" alt="">
                        <img src="{{asset('img/payment_png/6.png')}}" alt="">
                        <img src="{{asset('img/payment_png/7.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="last_footer"></div>
</footer>