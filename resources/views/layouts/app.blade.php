<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">


    @include('layouts.partials.htmlheader')




    @include('layouts.partials.css')


<body>


    @yield('main-content')





    @include('layouts.partials.scripts')


</body>
</html>