<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline_instance extends Model
{
    private static function getAirlinesBetweenCities($from_city_name, $to_city_name)
    {
        $from_city = City::where('name', $from_city_name)->first();

        $to_city = City::where('name', $to_city_name)->first();
        $to_city_id = $to_city->id;
//dd($to_city->id);
        $to_country = Country::where('id', $from_city->id)->first();
        $from_country = Country::where('id', $to_city->id)->first();
        $departure_airlines = $from_city->departure_airlines()->with('departure_airport.city', 'arrival_airport.city')->get();
        //dd($departure_airlines->toArray());
        $result = [];
        $i = 0;
        foreach ($departure_airlines as $airline){
            $airline_arrival_city = $airline->arrival_airport->city->id;
//            dd($airline);
            if ($airline_arrival_city == $to_city_id){
                $airline_instances = $airline
                    ->airline_instances()
                    ->with('airline.departure_airport', 'airline.arrival_airport', 'plane')
                    ->get();
//                dd($airline_instances);
                foreach ($airline_instances as $airline_instance){

                    $tickets = $airline_instance->tickets()->get();
                    //$tickets_count = $airline_instance->tickets->count();
                    $economy_tickets_count = 0;
                    $business_tickets_count = 0;
                    $plane_id = $airline_instance->plane_id;
//                    dd($plane_id);
                    $economy_seats_count = $airline_instance->plane->seats_economy_count;
                    $business_seats_count = $airline_instance->plane->seats_business_count;
                    $plane_name = $airline_instance->plane->name;
                    $plane_number = $airline_instance->plane->number;
                    //dd($tickets);


                    foreach ($tickets as $ticket){
                        if ($ticket->seat_number <= $business_seats_count)
                            $business_tickets_count++;
                        else
                            $economy_tickets_count++;
                    }

                    //dd($economy_tickets_count);
                    $result[$i]['id'] = $airline_instance->id;
                    $result[$i]['from_city'] = $from_city_name;
                    $result[$i]['to_city'] = $to_city_name;
                    $result[$i]['from_country'] = $from_country->name;
                    $result[$i]['to_country'] = $to_country->name;
                    $result[$i]['departure_airport'] = $airline_instance->airline->departure_airport->name;
                    $result[$i]['arrival_airport'] = $airline_instance->airline->arrival_airport->name;
                    $result[$i]['departure_time'] = $airline_instance->departure_time;
                    $result[$i]['arrival_time'] = $airline_instance->arrival_time;
                    $result[$i]['price_economy'] = $airline_instance->price_economy;
                    $result[$i]['price_business'] = $airline_instance->price_business;
                    $result[$i]['free_seats_economy'] = $economy_seats_count - $economy_tickets_count;
                    $result[$i]['free_seats_business'] = $business_seats_count - $business_tickets_count;
                    $result[$i]['plane_name'] = $plane_name;
                    $result[$i]['plane_number'] = $plane_number;
                    $i++;
                }
            }
        }
        return $result;
    }

    public function airline()
    {
        return $this->belongsTo(Airline::class);
    }

    public function plane()
    {
        return $this->belongsTo(Plane::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }




    public static function getAllBetweenCitiesByDate($from_city_name, $to_city_name, $date)
    {
        //strstr($airline_instance->departure_time, $date)

//        $from_city_name = 'Kijów';
//        $to_city_name = 'Warszawa';
//        $date = '2018-03-21';

        $airline_instances = self::getAirlinesBetweenCities($from_city_name, $to_city_name);

        $result = [];

        foreach ($airline_instances as $airline_instance){
            // TODO: test without date
            // if (strstr($airline_instance['departure_time'], $date)){
                $result[] = $airline_instance;
            // }
        }
        return $result;
    }
}
