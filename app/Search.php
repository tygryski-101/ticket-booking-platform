<?php

namespace App;

class Search
{
    public $items;

    public function __construct($oldSearch)
    {
        if($oldSearch){
            $this->items = $oldSearch->items;
        }else{
            $this->items = null;
        }
    }

    public function add($item){
        $this->items = $item;
    }
}
