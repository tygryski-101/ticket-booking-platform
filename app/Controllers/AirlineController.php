<?php

namespace App\Http\Controllers;

use App\Airline;
use App\Airline_instance;
use App\Airport;
use App\City;
use App\Mail\MailClass;
use App\Mail\MailTicketsClass;
use Illuminate\Support\Facades\Auth;
use App\Ticket;
use App\Country;
use App\Cart;
use App\Search;
use App\Plane;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AirlineController extends Controller
{

    public function index()
    {
        //        Session::flush();
        return view('/home');
    }

    public function showAirline($id)
    { }

    /*
        Show single airline
    */
    public function showAirlineInstance($id)
    { }


    public function create()
    { }

    public function store()
    { }

    public function edit()
    { }

    public function update()
    { }

    public function getCity(Request $request)
    {
        if ($request->from_to) {
            $search = $request->from_to;
            $city = (new \App\City)->where('name', 'like', $search . '%')->get();
            return response()->json([
                'status' => 'success',
                'city' => $city
            ]);
        }
    }
    public function preloderSearch(Request $request)
    {
        $airlines = $request->all();

        if ($request->one_two_airline == 2) {
            $this->validate($request, [
                'from_city' => 'required',
                'to_city' => 'required',
                'data_from' => 'required',
                'data_to' => 'required',
            ]);

            $to_city = City::where('name', $request->to_city)->first();
            $from_city = City::where('name', $request->from_city)->first();
            $to_country = Country::where('id', $from_city->id)->first();
            $from_country = Country::where('id', $to_city->id)->first();
            $airlines['from_country'] = $from_country->name;
            $airlines['to_country'] = $to_country->name;

            $oldSearch = Session::has('search') ? Session::get('search') : null;
            $search = new Search($oldSearch);
            $search->add($airlines);
            $request->session()->put('search', $search);

            return view('/preloader', compact('airlines'));
        } else {
            $this->validate($request, [
                'from_city' => 'required',
                'data_from' => 'required',
            ]);
            $to_city = City::where('name', $request->to_city)->first();
            $from_city = City::where('name', $request->from_city)->first();
            $to_country = Country::where('id', $from_city->id)->first();
            $from_country = Country::where('id', $to_city->id)->first();
            $airlines['from_country'] = $from_country->name;
            $airlines['to_country'] = $to_country->name;

            $oldSearch = Session::has('search') ? Session::get('search') : null;
            $search = new Search($oldSearch);
            $search->add($airlines);
            $request->session()->put('search', $search);

            return view('/preloader', compact('airlines'));
        }
    }

    public function searchAirlines(Request $request)
    {
        if (!Session::has('search')) {
            return view('/');
        }

        if (Session::has('cart')) {
            $old = Session::get('cart');
            $checked = array_keys($old->items);
        }
        $oldSearch = Session::get('search');

        $old_airlines = $oldSearch->items;

        $data_from = $old_airlines['data_from'];
        $data_to = $old_airlines['data_to'];
        $one_two_airline = $old_airlines['one_two_airline'];
        if ($one_two_airline == 2) {
            $airlines_from = Airline_instance::getAllBetweenCitiesByDate($old_airlines['from_city'], $old_airlines['to_city'], $data_from);
            $airlines_to = Airline_instance::getAllBetweenCitiesByDate($old_airlines['to_city'], $old_airlines['from_city'], $data_to);
            return view('/search_airlines', compact('airlines_to', 'airlines_from', 'one_two_airline', 'data_from', 'data_to', 'old_airlines', 'checked'));
        } else {
            $airlines_from = Airline_instance::getAllBetweenCitiesByDate($old_airlines['from_city'], $old_airlines['to_city'], $data_from);
            return view('/search_airlines', compact('airlines_from', 'one_two_airline', 'data_from', 'old_airlines', 'checked'));
        }
    }

    public function showCart(Request $request)
    {
        if (!Session::has('cart')) {
            return view('/cart');
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        //        dd($cart->items);
        return view('/cart', ['airlines' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    public function getAddToCart(Request $request, $id)
    {
        if (!$request->ajax()) {
            dd(1);
        }
        $airlines = Airline_instance::find($id);
        $plane = Plane::find($airlines->plane_id);
        $from_airport = Airport::find(Airline::find($airlines->airline_id)->from_airport_id);
        $to_airport = Airport::find(Airline::find($airlines->airline_id)->to_airport_id);
        $from_city = City::find($from_airport->city_id);
        $to_city = City::find($to_airport->city_id);
        $from_country = City::find($from_city->country_id);
        $to_country = City::find($to_city->country_id);
        $airlines['plane_name'] = $plane->name;
        $airlines['plane_number'] = $plane->number;
        $airlines['from_airport'] = $from_airport->name;
        $airlines['to_airport'] = $to_airport->name;
        $airlines['from_city'] = $from_city->name;
        $airlines['to_city'] = $to_city->name;
        $airlines['from_country'] = $from_country->name;
        $airlines['to_country'] = $to_country->name;

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($airlines, $airlines->id);

        $request->session()->put('cart', $cart);
        //        dd($request->session()->get('cart'));
        return response()->json([
            'location' => '/search_airlines',
            'status' => 'success'
        ]);
        //        return redirect()->route('airlines.index');
    }

    public function getDeleteToCart(Request $request, $id)
    {
        if (!$request->ajax()) {
            dd(111);
        }
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $cart->delete($id);

        $request->session()->put('cart', $cart);

        return response()->json([
            'location' => '/search_airlines',
            'status' => 'success'
        ]);
    }

    public function checkoutCart(Request $request)
    {
        $airlines = $request->airline_;
        $classes = $request->class_;
        foreach ($airlines as $key => $value) {
            $id_airlines_price[] = $value;
        }
        $ticket_ids = [];
        foreach ($classes as $key => $value) {
            $class = isset($value) ? 'business' : 'economy';
            $airline = Airline_instance::with('plane', 'tickets')->findOrFail($key);
            $seat = Ticket::getSeat($class, $airline);
            if ($seat != false) {
                $data['name'] = $request->first_name;
                $data['surname'] = $request->last_name;
                $data['email'] = $request->email;
                $data['phone'] = $request->tel;
                $data['airline_instance_id'] = $key;
                $data['seat_number'] = $seat;
                if (Auth::check()) {
                    $data['user_id'] = Auth::id();
                } else {
                    $data['user_id'] = null;
                }
                $data['cancel_code'] = str_random(60);

                $ticket = new Ticket();
                $ticket->fill($data)->save();
                $ticket_ids[] = $ticket->id;
                $bought = true;
                $message = 'Your ticket was successfully purchased';
            } else {
                $bought = false;
                $message = 'No free seats';
                return view('/cart', compact('bought', 'message'));
            }
        }
        $tickets_airlines = Ticket::getAllDateOfTickets($ticket_ids);
        Mail::to($data['email'])->send(new MailTicketsClass($data['name'], $data['surname'], $data['email'], $tickets_airlines));
        Session::flush();
        return view('/cart', compact('bought', 'message'));
    }

    public function checkProfile(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required|max:150',
                'last_name' => 'required|max:100',
                'email' => 'required|email',
                'tel' => 'required|regex:/[0-9]{6}/',
            ],
            []
        );
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages(),
                'status' => 'error'
            ]);
        }
        return response()->json([
            'status' => 'success',
            'msg' => $request->all()
        ]);
    }

    //airline instance id must be in request like airline_instance_id
    public function reserve(Request $request)
    {

        $airline_instance = Airline_instance::with('plane', 'tickets')->findOrFail($request->airline_instance_id);

        if (!$airline_instance)
            dd('Not existing airline instance');

        $data = $request->all();
        $seat = Ticket::getSeat($request->seat_class, $airline_instance);
        if ($seat != false)
            $data['seat_number'] = $seat;
        else
            dd('No free seats');

        if (Auth::check())
            $data['user_id'] = Auth::id();

        $data['cancel_code'] = str_random(60); //using this code user can cancel reservation without auth (specially for unregistered users)

        $ticket = new Ticket();
        $ticket->fill($data)->save();
    }
    public function cancelReservation(Request $request = null, $id = null)
    {
        //request must have id and cancel_code
        if (!$request) {
            $ticket = Ticket::where(['id', $request->id], ['cancel_code', $request->cancel_code])->find();
            $ticket->delete();
            dd('Reservation was canceled by cancel_code');
        } else {
            $ticket = Ticket::with('user')->find($id);
            //dd($ticket->user != null);
            if ($ticket->user != null) {
                if (Auth::id() == $ticket->user->id) {
                    $ticket->delete();
                    dd('Auth user canceled the reservation');
                } else {
                    dd('You are not owner of this ticket');
                }
            } else {
                dd('This ticket has not registered owner');
            }
        }
    }
    public function getUser()
    {
        if (!Auth::check())
            return view('/login');

        $user = Auth::user();

        $tickets_airlines = Ticket::getAllTicketsOfUser($user->id);

        return view('/profile', compact('user', 'tickets_airlines'));
    }
    public function deleteTicket($id)
    {
        DB::beginTransaction();
        try {
            $ticket = Ticket::find($id);
            $ticket->destroy($id);

            $msg = [
                'status' => "success",
                'message' => "The reservation was cancelled successfully"
            ];

            DB::commit();
            return response()->json($msg, 200);
        } catch (Exception $ex) {
            DB::rollBack();
            return response()->json('error', 400);
        }
    }
}
