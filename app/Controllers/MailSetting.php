<?php

namespace App\Http\Controllers;


use App\Mail\MailClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
class MailSetting extends Controller
{

    public function sendMail(Request $request){
        $validator = Validator::make(Input::all(), [
            'inputName' => 'required|max:50',
            'inputEmail' => 'required|email',
            'inputmessage' => 'required|max:500',
//            'inputPhone' => 'required|min:9|regex:/[0-9]{9}/'
        ]);
        if ($validator->fails()) {
            $msg = $validator->messages();
            return response()->json([
                'message' => $msg
            ], 422);
        }

        $inputName = $request->inputName;
        $inputEmail = $request->inputEmail;
        $inputmessage = $request->inputmessage;
        $inputPhone = $request->inputPhone;

        Mail::to('manoliyvlad@gmail.com')->send(new MailClass($inputName, $inputEmail, $inputmessage, $inputPhone));


        return response()->json([
            'message' => 'Your message has been successfully sent!'
        ], 200);

    }
}

