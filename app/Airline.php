<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    public function departure_airport()
    {
        return $this->belongsTo(Airport::class, 'from_airport_id');
    }

    public function arrival_airport()
    {
        return $this->belongsTo(Airport::class, 'to_airport_id');
    }

    public function airline_instances()
    {
        return $this->hasMany(Airline_instance::class);
    }


    public static function getFromCity($city)
    {
        // TODO
    }

    public static function getAirlinesToCity($city)
    {
        // TODO
    }
}
