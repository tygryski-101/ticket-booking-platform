<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone',
        'airline_instance_id',
        'seat_number',
        'cancel_code',
        'user_id'
    ];
    public function airline_instance()
    {
        return $this->belongsTo(Airline_instance::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public static function getAllTicketsOfUser($id){
        $tickets = Ticket::where('user_id', '=', $id)->get();

        $tickets_airlines = [];
        $i = 0;
        foreach ($tickets as $ticket){
            $airline_instance = Airline_instance::where('id', $ticket->airline_instance_id)->with('airline', 'plane')->first();
            $tickets_airlines[$i]['airline_instance'] = $airline_instance;

            $airports_from = Airport::where('id', $airline_instance->airline->from_airport_id)->first();
            $tickets_airlines[$i]['airports_from'] = $airports_from;
            $airports_to = Airport::where('id', $airline_instance->airline->to_airport_id)->first();
            $tickets_airlines[$i]['airports_to'] = $airports_to;

            $city_from = City::where('id', $airports_from->city_id)->first();
            $tickets_airlines[$i]['city_from'] = $city_from;
            $city_to = City::where('id', $airports_to->city_id)->first();
            $tickets_airlines[$i]['city_to'] = $city_to;

            $country_from = Country::where('id', $city_from->country_id)->first();
            $tickets_airlines[$i]['country_from'] = $country_from;
            $country_to = Country::where('id', $city_to->country_id)->first();
            $tickets_airlines[$i]['country_to'] = $country_to;

            $tickets_airlines[$i]['ticket'] = $ticket;
            $i++;
        }
        return $tickets_airlines;
    }
    public static function getAllDateOfTickets($ids){
        $tickets = [];

        foreach ($ids as $id){
            $tickets [] = Ticket::where('id', $id)->first();
        }

        $tickets_airlines = [];
        $i = 0;
        foreach ($tickets as $ticket){
            $airline_instance = Airline_instance::where('id', $ticket->airline_instance_id)->with('airline', 'plane')->first();
            $tickets_airlines[$i]['airline_instance'] = $airline_instance;

            $airports_from = Airport::where('id', $airline_instance->airline->from_airport_id)->first();
            $tickets_airlines[$i]['airports_from'] = $airports_from;
            $airports_to = Airport::where('id', $airline_instance->airline->to_airport_id)->first();
            $tickets_airlines[$i]['airports_to'] = $airports_to;

            $city_from = City::where('id', $airports_from->city_id)->first();
            $tickets_airlines[$i]['city_from'] = $city_from;
            $city_to = City::where('id', $airports_to->city_id)->first();
            $tickets_airlines[$i]['city_to'] = $city_to;

            $country_from = Country::where('id', $city_from->country_id)->first();
            $tickets_airlines[$i]['country_from'] = $country_from;
            $country_to = Country::where('id', $city_to->country_id)->first();
            $tickets_airlines[$i]['country_to'] = $country_to;

            $tickets_airlines[$i]['ticket'] = $ticket;
            $i++;
        }
        return $tickets_airlines;

    }
    public static function getSeat($seat_class, $airline_instance){

        $seats_economy_count = $airline_instance->plane->seats_economy_count;
        $seats_business_count = $airline_instance->plane->seats_business_count;
        $reserved_seats = $airline_instance->tickets->pluck('seat_number');
        $seat = null;
        $seat_array = [];

//first seats - business, next economy
        if ($seat_class == 'economy'){
            for ($i = $seats_business_count+1; $i <= $seats_economy_count; $i++){
                $seat_array[] = $i;
            }
            $free_seats = array_diff($seat_array, $reserved_seats->toArray());

            if (!empty($free_seats))
                return reset($free_seats);
        }

        if($seat_class == 'business'){

            for ($i = 1; $i <= $seats_business_count; $i++){
                $seat_array[] = $i;
            }

            $free_seats = array_diff($seat_array, $reserved_seats->toArray());
            if (!empty($free_seats))
                return reset($free_seats);
        }

        return false;
    }

}
