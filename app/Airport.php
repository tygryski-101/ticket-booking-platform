<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function departures()
    {
        return $this->hasMany(Airline::class , 'from_airport_id');
    }

    public function arrivals()
    {
        return $this->hasMany(Airline::class, 'to_airport_id');
    }

    public function departure_airline_instances()
    {
        return $this->hasManyThrough(Airline_instance::class, Airline::class, 'from_airport_id');
    }

    public function arrival_airline_instances()
    {
        return $this->hasManyThrough(Airline_instance::class, Airline::class, 'to_airport_id');
    }
}
