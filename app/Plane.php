<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plane extends Model
{
    public function airline_instances()
    {
        return $this->hasMany(Airline_instance::class);
    }
    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, Airline_instance::class);
    }
}
